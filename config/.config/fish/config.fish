set TERM xterm

# for rust
set fish_user_paths /home/sohomb/.cargo/bin $fish_user_paths

#for python
set fish_user_paths /home/sohomb/.local/bin $fish_user_paths

# for go
set fish_user_paths /home/sohomb/work_space/go/lib/bin $fish_user_paths
#set fish_user_paths /home/sohomb/work_space/go/code/bin $fish_user_paths

set -x GOPATH /home/sohomb/work_space/go/lib /home/sohomb/work_space/go/code
